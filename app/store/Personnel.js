Ext.define('pertemuan.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',

    alias: 'store.personnel',

    fields: [
        'photo', 'npm', 'name', 'email', 'phone'
    ],

    data: { items: [
        { photo : '<img width=100px height=150px src="../../../resources/bg.png"/>', jurusan:'informatika', npm : '183510356', name: 'Raviq Zerhar', email: "raviqzerhar@student.uir.ac.id", phone: "555-111-1111" },
        { photo : '<img width=100px height=150px src="../../../resources/1.jpg"/>',  jurusan:'pertanian',   npm : '183510354', name: 'budi tabuti',   email: "budi01@gmail.com.com",  phone: "555-111-1111" },
        { photo : '<img width=100px height=150px src="../../../resources/2.jpg"/>',  jurusan:'pertanian',   npm : '183510351', name: 'andre',         email: "andre02@gmail.com",     phone: "555-222-2222" },
        { photo : '<img width=100px height=150px src="../../../resources/3.jpg"/>',  jurusan:'informatika', npm : '183510352', name: 'daniel',        email: "daniel03@gmail.com",    phone: "555-333-3333" },
        { photo : '<img width=100px height=150px src="../../../resources/4.jpg"/>',  jurusan:'hukum',       npm : '183510353', name: 'gilang',        email: "daniel@gmail.com",      phone: "555-444-4444" }
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
