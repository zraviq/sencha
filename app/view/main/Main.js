/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('pertemuan.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',
        
        'pertemuan.view.main.MainController',
        'pertemuan.view.main.MainModel',
        'pertemuan.view.main.List',
        'pertemuan.view.form.user',
        'pertemuan.view.grup.Carousel',
        'pertemuan.view.setting.BasicDataView',
        'pertemuan.view.form.login',
        'pertemuan.view.chart.Tab',
        'pertemuan.view.chart.Area',
        'pertemuan.view.tree.TreePanel'
        
        
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'mainlist'
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'user'
            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
           items: [{
                xtype: 'mycarousel'
            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
           items: [{
                xtype: 'basicdataview'
            }]
        },
        {
            title: 'chart',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
           items: [{
                xtype: 'charttab'
            }]
        },
        {
            title: 'Tree',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
           items: [{
                xtype: 'tree-panel'
            }]
        },
    ]
});
