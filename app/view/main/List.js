/**
 * This view is an example list of people.
 */
Ext.define('pertemuan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'pertemuan.store.Personnel'
    ],

    title: 'data anggota sencha study club raviq',

    store: {
        type: 'personnel'
    },

    columns: [
        
        { text: 'NPM',  dataIndex: 'npm', width: 100 },
        { text: 'Nama',  dataIndex: 'name', width: 100 },
        { text: 'Email', dataIndex: 'email', width: 230 },
        { text: 'telepon', dataIndex: 'phone', width: 150 }
    ],

    listeners: {
        select: 'onDataDipilih'
    }
});
