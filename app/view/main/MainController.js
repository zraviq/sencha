/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('pertemuan.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onDataDipilih: function (sender, record) {
        var nama = record.data.name;
        var npm = record.data.npm;
        localStorage.setItem('nama', nama);
        localStorage.setItem('npm', npm);
        Ext.Msg.confirm('konfirmasi', 'yakin '+nama+'?', 'onConfirm', this);
        console.log(record.data);
    },

    onConfirm: function (choice) {
        var nama = localStorage.getItem('nama');
        var npm = localStorage.getItem('npm');
        if (choice === 'yes') {
            alert('terimakasih telah memilih yes '+nama+'('+npm+')')
        }
        else{
            alert('JANGAN MEMILIH NOOO '+nama+'('+npm+')');
        }
    }
});
