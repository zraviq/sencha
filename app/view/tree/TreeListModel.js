Ext.define('pertemuan.view.tree.TreeListModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tree-list',

    formulas: {
        selectionText: function(get) {
            var selection = get('treelist.selection'),
                path;
            if (selection) {
                path = selection.getPath('text');
                path = path.replace(/^\/Root/, '');
                return 'Selected: ' + path;
            } else {
                return 'No node selected';
            }
        }
    }, 

    stores: {
        navItems: {
            type: 'tree',
            rootVisible: true,
            root: {
                expanded: true,
                text: 'informatika',
                iconCls: 'x-fa fa-sitemap',
                children: [{
                    expanded: true,
                    text: 'pertanian',
                    iconCls: 'x-fa fa-sitemap',
                    children:[{
                        text: 'hukum',
                        iconCls: 'x-fa fa-sitemap'
                    }]
                }]
            }
        }
    }
});