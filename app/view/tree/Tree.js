Ext.define('pertemuan.view.tree.Tree', {
    extend: 'Ext.grid.Tree',
    xtype: 'tree-list',
    requires: [
        'Ext.grid.plugin.MultiSelection'
    ],

    
    cls: 'demo-solid-background',
    shadow: true,

    viewModel: {
        type: 'tree-list'
    },

    bind: '{navItems}',
    listeners: {
        itemtap: function(me, index, terget, record, e, eOpts){
            stor = Ext.getStore('personnel');
            stor.filter('jurusan',record.data.text);
        }
    }
});