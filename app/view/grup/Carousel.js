Ext.define('pertemuan.view.grup.Carousel', {
    extend: 'Ext.Container',
    xtype: 'mycarousel',

    requires: [
        'Ext.carousel.Carousel'
    ],

    cls: 'cards',
    shadow: true,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    items: [{
        xtype: 'carousel',
        flex: 1,
        items: [{
            html: '<p>Swipe left to show the next card…</p>',
            cls: 'card'
        },
        {
            html: '<p>You can also tap on either side of the indicators.</p>',
            cls: 'card'
        },
        {
            html: 'Card #3',
            cls: 'card'
        }]
    },{
        
                xtype: 'tab',
                flex: 4,
           
    }, {
        xtype: 'carousel',
        flex: 1,
        ui: 'light',
        direction: 'vertical',
        items: [{
            html: '<p>Carousels can also be vertical <em>(swipe up)…</p>',
            cls: 'card dark'
        },
        {
            html: 'And can also use <code>ui:light</code>.',
            cls: 'card dark'
        },
        {
            html: 'Card #3',
            cls: 'card dark'
        }]
    }]
});