Ext.define('pertemuan.view.setting.BasicDataView', {
    extend: 'Ext.Container',
    xtype: 'basicdataview',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'pertemuan.store.Personnel',
        'Ext.field.Search'
    ],

    viewModel:{
        stores:{
            personnel:{
                type: 'personnel'
            }
        }
    },
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [
        {
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'searchfield',
                    placeHolder: 'NAMA',
                    name: 'searchfield',
                    listeners: {
                        change: function (me, newValue, oldValue, eOpts){
                            personnelStore= Ext.getStore('personnel');
                            personnelStore.filter('name', newValue);
                        }
                    }
                },
                {
                    xtype: 'searchfield',
                    placeHolder: 'NPM',
                    name: 'searchfield',
                    listeners: {
                        change: function (me, newValue, oldValue, eOpts){
                            personnelStore= Ext.getStore('personnel');
                            personnelStore.filter('npm', newValue);
                        }
                    }
                },
                {
                    xtype: 'searchfield',
                    placeHolder: 'EMAIL',
                    name: 'searchfield',
                    listeners: {
                        change: function (me, newValue, oldValue, eOpts){
                            personnelStore= Ext.getStore('personnel');
                            personnelStore.filter('email', newValue);
                        }
                    }
                },
            ]
        },
        {
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl: '<div class="card" style="width:300px">{photo}<br> {npm}<br> <b><font size=4>{name}</font></b><br> <i><font color=#0066cc>{email}</font></i><br> <u>{phone}</u><hr></div>',
        bind: {
            store: '{personnel}',
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            //delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>npm: </td><td>{npm}</td></tr>' +
                    '<tr><td>name:</td><td>{name}</td></tr>' +
                    '<tr><td>email:</td><td>{email}</td></tr>' +
                    '<tr><td>phone:</td><td>{phone}</td></tr>'
                    
        }
    }]
});