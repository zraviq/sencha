Ext.define('pertemuan.view.setting.Setting', {
    extend: 'Ext.tab.Panel',
    xtype: 'tab',

    shadow: true,
    cls: 'demo-solid-background',
    tabBar: {
        layout: {
            pack: 'center'
        }
    },
    activeTab: 1,
    defaults: {
        scrollable: true
    },
    items: [
        {
            title: 'hbox',
            xtype: 'ctab1',
            cls: 'card'
        },
        {
            title: 'vbox',
            xtype: 'ctab2',
            cls: 'card'
        }
    ]
});