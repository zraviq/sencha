Ext.define('pertemuan.view.form.login', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Password'
    ],
    shadow: true,
    cls: 'demo-solid-background',
    xtype: 'login',
    controller: 'login',
    id: 'login',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldsetlogin',
            title: 'Login Form',
            instructions: 'Please enter the information above.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'username',
                    label: 'User Name',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'pw',
                    label: 'Password',
                    clearIcon: true
                },
                
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'login',
                    ui: 'action',
                    hasDisabled: false,
                    handler: 'onLogin'
                },
                
                

            ]
        }
    ]
});