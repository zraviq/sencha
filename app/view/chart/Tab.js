Ext.define('pertemuan.view.chart.Tab', {
    extend: 'Ext.tab.Panel',
    xtype: 'charttab',

    shadow: true,
    cls: 'demo-solid-background',
    tabBar: {
        layout: {
            pack: 'center'
        }
    },
    activeTab: 1,
    defaults: {
        scrollable: true
    },
    items: [
        {
            title: 'CHART 1',
            xtype: 'renderer',
            cls: 'card'
        },
        {
            title: 'CHART 2',
            xtype: 'area',
            cls: 'card'
        }
    ]
});